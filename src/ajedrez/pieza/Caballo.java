/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ajedrez.pieza;

import ajedrez.coordenada.Coordenada;
import ajedrez.historial.Historial;

/**
 *
 * @author Administrador
 */
public class Caballo implements Pieza{
    private Coordenada posicionActual;
    private Historial historial ;
    public Caballo (Coordenada posicionInicial){
        if(posicionInicial==null)
            throw new IllegalArgumentException("La posicion es null");
        historial=Historial.getInstancia();
        posicionActual = posicionInicial;
       
    }
    
    public void ajustarHistorial(int filas){
        historial.borrarHistorialDentro(filas);
    }
    
    
    @Override
    public boolean moverPieza(Coordenada nuevaPosicion) {
        if( validarMovimiento(nuevaPosicion)){
           if(historial.getTamaño().equals(0))
                historial.agregarMovimiento(new Caballo(posicionActual));
           posicionActual = nuevaPosicion;
           historial.agregarMovimiento(new Caballo(nuevaPosicion));
           return true;
        }
        
        return false;
    }
    
    @Override
    public void regresarMovimiento(){
        posicionActual=historial.regresarMovimiento().getPosicionActual();
      
    }
    
    
    @Override 
    public void siguienteMovimiento(){
        posicionActual=historial.siguienteMovimiento().getPosicionActual();
    }
    
    private boolean validarMovimiento(Coordenada nuevaPosicion){
        return (Math.abs(posicionActual.getFila() - nuevaPosicion.getFila()) == 1 && Math.abs(posicionActual.getColumna()- nuevaPosicion.getColumna()) == 2 )
                || (Math.abs(posicionActual.getFila() - nuevaPosicion.getFila()) == 2 && Math.abs(posicionActual.getColumna()- nuevaPosicion.getColumna()) == 1) ;
    }
    
    @Override
    public Coordenada getPosicionActual() {
        return posicionActual;
    }
    
    @Override
    public String toString() {
        return "Caballo{" + "posicionActual :=" + posicionActual + 
                        "\n historial:="+historial+
               "}";
    }
    
    
}
