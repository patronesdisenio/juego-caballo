/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ajedrez.pieza;

import ajedrez.coordenada.Coordenada;

/**
 *
 * @author Administrador
 */
public interface Pieza {
    public boolean moverPieza(Coordenada coordenada);
    public Coordenada getPosicionActual();
    public void regresarMovimiento();
    public void siguienteMovimiento();
    
}
