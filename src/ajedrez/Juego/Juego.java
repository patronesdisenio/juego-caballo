/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ajedrez.Juego;

import ajedrez.coordenada.Coordenada;
import ajedrez.historial.Historial;
import ajedrez.pieza.Caballo;
import ajedrez.pieza.Pieza;
import ajedrez.tablero.Tablero;

/**
 *
 * @author Administrador
 */
public class Juego {
    private Tablero tablero;
    private Pieza pieza;
    public Juego(int filas, Coordenada coordenadaPieza){
        pieza = new Caballo(coordenadaPieza);
       
        if(coordenadaPieza.getColumna() > filas || coordenadaPieza.getFila() > filas)
            throw new IllegalArgumentException("La pieza esta en una posición fuera del tablero");
        tablero = new Tablero(filas);
       tablero.ocuparCasilla(coordenadaPieza);
    }
    
    public boolean moverPieza(Coordenada nuevaCordenada){
       try{ 
           if(tablero.ocuparCasilla(nuevaCordenada)){
                if(pieza.moverPieza(nuevaCordenada)){
                   return true;
                }
                else{
                    tablero.desocuparCasilla(nuevaCordenada);
                   
                }
           }
           
       }
       catch(IllegalArgumentException ilegal){
           return false;
       }
       return false;
    }
    
    public boolean reducirTablero(int decremento){
        
        return tablero.reducirTablero(decremento);
    }
    
    public boolean incrementarTablero(int incremento){
       return tablero.agrandarTablero(incremento);
    }
    
     
    public void regresarMovimiento(){
        tablero.desocuparCasilla(pieza.getPosicionActual());
        pieza.regresarMovimiento();
    }
    
    public void siguienteMovimiento(){
        pieza.siguienteMovimiento();
        tablero.ocuparCasilla(pieza.getPosicionActual());
    }

    @Override
    public String toString() {
        return "Juego{" + "\ntablero=" + tablero  + ",\n pieza=" + pieza + '}';
    }
    
    public void imprimirTablero(){
       tablero.imprimirCasillas();
    }

}
