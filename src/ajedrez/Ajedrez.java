/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ajedrez;

import ajedrez.Juego.Juego;
import ajedrez.casilla.Casilla;
import ajedrez.coordenada.Coordenada;
import ajedrez.pieza.Caballo;
import ajedrez.pieza.Pieza;
import ajedrez.tablero.Tablero;

/**
 *
 * @author Administrador
 */
public class Ajedrez {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Juego juegoAjedrez=new Juego(10,new Coordenada(1, 8));
        System.out.println(juegoAjedrez.moverPieza(new Coordenada(2, 6)));
        System.out.println(juegoAjedrez.moverPieza(new Coordenada(4, 7)));
        System.out.println(juegoAjedrez.moverPieza(new Coordenada(3, 5)));
        System.out.println(juegoAjedrez.moverPieza(new Coordenada(1, 4)));
        System.out.println(juegoAjedrez.moverPieza(new Coordenada(2, 2)));
        System.out.println(juegoAjedrez);
        
        juegoAjedrez.imprimirTablero();
        
        System.out.println("===================================== \n");
        System.out.println(juegoAjedrez.moverPieza(new Coordenada(4, 1)));
        juegoAjedrez.imprimirTablero();
        System.out.println("===================================== \n");
       
        juegoAjedrez.reducirTablero(1);
        juegoAjedrez.imprimirTablero();
        
      
      
    }
    
}
