/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ajedrez.casilla;

/**
 *
 * @author Administrador
 */
public class Casilla {
    private boolean visitado;

    public Casilla() {
        this.visitado = false;
    }
    
    
    public boolean isVisitado(){
        return visitado;
    }
    
    public void cambiarEstadoVisitar(){
        visitado = !visitado;
    }

    @Override
    public String toString() {
        return "Casilla{" + "visitado=" + visitado + '}';
    }
    
    
}
