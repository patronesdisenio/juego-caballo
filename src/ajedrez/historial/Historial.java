/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ajedrez.historial;

import ajedrez.pieza.Pieza;
import java.util.Stack;

/**
 *
 * @author Administrador
 */
public class Historial {
     private static Stack <Pieza> movimientos;
     private static Historial historial;
     private Integer indice;
    private Historial(){
          movimientos = new Stack();
          indice=0;
    }
     
    public static  Historial getInstancia(){
        if(historial==null)
            historial=new Historial();
        return historial;
    }
    
    
    public void agregarMovimiento(Pieza pieza){
        eliminarPiezasDespuesIndice();
        movimientos.add(pieza);
        indice++;
    }
     
    public Pieza regresarMovimiento(){
        indice--;
        if (indice.equals(0))
             return retornarPrimerPieza();
       
        
        return piezaIndice(indice);
       
    }
    public Pieza siguienteMovimiento(){
        indice++;
        if(indice >= movimientos.size()){
           
            return retornarUltimaPieza();
        }
        return piezaIndice(indice);
    }
    
    
    
    
     
    private void eliminarPiezasDespuesIndice(){
        if(indice.equals(movimientos.size()))
            return;
        this.movimientos=eliminarPiezasIndice();
    }
    
    
    private Stack <Pieza>  eliminarPiezasIndice(){
        Stack <Pieza> movimientostmp=this.movimientos;
        Stack <Pieza> nuevaPila = new Stack();;
        Integer numeroIndice=0;
        while(!movimientostmp.isEmpty()){
            if(numeroIndice<=this.indice){
                nuevaPila.add(movimientostmp.elementAt(numeroIndice));
                numeroIndice++;
            }
        
        }
        return nuevaPila;
    }
    
    
    public void borrarHistorialDentro(int filas){
        movimientos=eliminarPiezasDe(filas);
        
    }
    
    
    private  Stack<Pieza> eliminarPiezasDe(int filas){
        Stack<Pieza> movimientoTmp=movimientos;
        Integer contador=0;
        Stack<Pieza> nuevoHistorial=new Stack<>();
        while(!movimientoTmp.isEmpty()){
            Pieza piezaTmp=movimientoTmp.pop();
            if(piezaTmp.getPosicionActual().getColumna()< filas || piezaTmp.getPosicionActual().getColumna()< filas ){
               indice=contador;
               movimientoTmp=eliminarPiezasIndice();
            }
        }
        
        return nuevoHistorial;
    }
    
    
    
    private Pieza retornarPrimerPieza(){
        Integer numeroPieza=movimientos.size();
        Stack <Pieza> movimientostmp=this.movimientos;
        return movimientostmp.firstElement();
     }
   
    private Pieza retornarUltimaPieza(){
       Stack <Pieza> movimientostmp=this.movimientos;  
       return movimientostmp.elementAt(movimientostmp.size()-1);
    }
     
     private Pieza piezaIndice(Integer indice){
        Integer numeroPieza=0;
        Stack <Pieza> movimientostmp=this.movimientos;
        while(!movimientostmp.isEmpty()){
            if(indice.equals(numeroPieza))
                return movimientostmp.elementAt(numeroPieza);
            numeroPieza++;
        }
        return null;
     }
     
     public Integer getTamaño(){
         return movimientos.size();
     }
     
     @Override
     public String toString(){
         return "Historial {"+
                 "tamaño:"+this.movimientos.size()+
                 "\nindice :"+this.indice+
                 "}";
     }
     
     
     
     
     
}
