/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ajedrez.tablero;

import ajedrez.casilla.Casilla;
import ajedrez.coordenada.Coordenada;
import ajedrez.pieza.Caballo;
import ajedrez.pieza.Pieza;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 *
 * @author Administrador
 */
public class Tablero {
    public Map <Coordenada, Casilla> casillas;
    private final int TAMAÑO_MINIMO=8;
    
    private int filaInicial, filaFinal, columnaInicial, columnaFinal;
    
    public Tablero (int filas){
        if(filas < TAMAÑO_MINIMO)
            filas = TAMAÑO_MINIMO;
        filaInicial = 1;
        columnaInicial = 1;
        this.filaFinal = filas;
        this.columnaFinal = filas;
        casillas = new HashMap();
        construirTablero();
       
       
        
    }
    
   
    
    private void construirTablero(){
        for(int i = filaInicial; i <= filaFinal; i ++)
            for( int j = columnaInicial; j <= columnaFinal ; j++){
                Coordenada temporal = new Coordenada(i,j);
                if(!casillas.containsKey(temporal))
                    casillas.put(temporal,new Casilla());
            }
    }
    
    public boolean estaEnMarcoReducido(Coordenada cordenada,int decremento){
        return cordenada.getFila() < filaInicial + decremento ||
           cordenada.getFila() > filaFinal - decremento ||
           cordenada.getColumna() < columnaInicial + decremento ||
           cordenada.getColumna() > columnaFinal - decremento;
    
    }
    
    public boolean agrandarTablero(int incremento){
        if(incremento <= 0)
            return false;
        filaFinal += incremento;
        columnaFinal += incremento;
        filaInicial -= incremento;
        columnaInicial -= incremento;
        construirTablero();
        return true;
    }
    
    public boolean reducirTablero(int decremento){
        if(decremento <= 0 || filaFinal - decremento <TAMAÑO_MINIMO || columnaFinal - decremento < TAMAÑO_MINIMO)
            return false;
        
        auxiliarReducirTablero(decremento);        
        filaFinal -= decremento;
        columnaFinal -= decremento;
        filaInicial += decremento;
        columnaInicial += decremento;
        return true;
    }
    
    private void auxiliarReducirTablero(int decremento){
        for(int i = filaInicial; i <= filaFinal; i ++)
            for( int j = columnaInicial; j <= columnaFinal ; j++){
                if(i < filaInicial + decremento || i > filaFinal - decremento || j < columnaInicial + decremento || j > columnaFinal - decremento){
                    Coordenada temporal = new Coordenada(i,j);
                    casillas.remove(temporal);
                }
            }
    }
    
    public boolean ocuparCasilla(Coordenada nuevaPosicion){
        if(!casillas.containsKey(nuevaPosicion))
            return false;
        if( casillas.get(nuevaPosicion).isVisitado())
           return false;
        casillas.get(nuevaPosicion).cambiarEstadoVisitar();
        return true;
    }
    
    public boolean desocuparCasilla(Coordenada nuevaPosicion){
        if(!casillas.containsKey(nuevaPosicion))
            return false;
        if(!casillas.get(nuevaPosicion).isVisitado())
           return false;
        casillas.get(nuevaPosicion).cambiarEstadoVisitar();
        return true;
    
    }
    
    public boolean todasLasCasillasOcupadas(){
       //pendiente
       return true;
    }
    public void imprimirCasillas(){
        for(int i = filaInicial; i <= filaFinal; i ++){
            for( int j = columnaInicial; j <= columnaFinal ; j++){
                Coordenada temporal = new Coordenada(i,j);
                if(casillas.get(temporal).isVisitado())
                    System.out.print(" ♞ ");
                else
                    System.out.print(" . ");
                
            }
            
            System.out.println("");
           }
    }
    
    
    
    @Override
    public String toString() {
        return "Tablero{" + "casillas=" + casillas.size() + ", filaInicial=" + filaInicial + ", filaFinal=" + filaFinal + ", columnaInicial=" + columnaInicial + ", columnaFinal=" + columnaFinal + '}';
    }
    

    
    
    
    
    
}
