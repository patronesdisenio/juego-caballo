/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ajedrez.coordenada;

import java.util.Objects;

/**
 *
 * @author Administrador
 */
public class Coordenada {
    
    private Integer fila;
    private Integer columna;

    public Coordenada(Integer fila, Integer columna) {
        
        this.fila = fila;
        this.columna = columna;
    }

    public Integer getFila() {
        return fila;
    }

    public Integer getColumna() {
        return columna;
    }

    @Override
    public String toString() {
        return "Coordenada{" + "fila=" + fila + ", columna=" + columna + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.fila);
        hash = 97 * hash + Objects.hashCode(this.columna);
        return hash;
    }

    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Coordenada other = (Coordenada) obj;
        if (!Objects.equals(this.fila, other.fila)) {
            return false;
        }
        if (!Objects.equals(this.columna, other.columna)) {
            return false;
        }
        return true;
    }
    
    
    
}
